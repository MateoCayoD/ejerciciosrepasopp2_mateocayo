using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sifelse : MonoBehaviour
{
    public int x;
    public int y;

    // Start is called before the first frame update
    void Start()
    {
        if (x < y)
        {
            Debug.Log("El segundo número es mayor");
        }

        else if (x == y)
        {
            Debug.Log("Ambos números son iguales");
        }

        else 
        {
            Debug.Log("El primer número es mayor");
        }
    }
}
