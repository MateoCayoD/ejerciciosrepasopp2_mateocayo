using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sconstructor : MonoBehaviour
{
    public string nombre;
    public int salud;
    // Start is called before the first frame update
    void Start()
    {
        Personaje Mago = new Personaje(nombre, salud);

        Debug.Log("nombre del personaje: " + Mago.Nombre);
        Debug.Log("salud del personaje: " + Mago.Salud);
    }
}

public class Personaje
{
    public string Nombre { get; private set; }
    public int Salud { get; private set; }

    public Personaje(string nombre, int salud)
    {
        Nombre = nombre;
        Salud = salud;
    }
}
